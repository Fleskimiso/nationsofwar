package digital.hiett.nationsofwar.server.listeners;

import digital.hiett.nationsofwar.server.Main;
import digital.hiett.nationsofwar.server.users.User;
import digital.hiett.nationsofwar.shared.events.UserEnterMatchmakingEvent;
import digital.hiett.nationsofwar.shared.packets.Packet;

import java.util.UUID;

public class OnUserEnterMatchmaking extends UserEnterMatchmakingEvent {

    @Override
    public void onEventReceive(UUID sender, String queueType) {
        // Check if they actually are online
        if (Main.getInstance().getUserManager().get(sender) == null
                || Main.getInstance().getMatchmakingManager().inQueue(sender))
            return;

        System.out.println("Entering " + sender + " into " + queueType + " matchmaking.");

        Main.getInstance().getMatchmakingManager().addToQueue(sender);

        // Send a packet that says the queue size.
        User user = Main.getInstance().getUserManager().get(sender);
        user.sendPacket(Packet.QUEUE_SIZE.build("" + Main.getInstance().getMatchmakingManager().getQueue().size()));

        // Update the queue
        Main.getInstance().getMatchmakingManager().updateQueue();
    }

}
