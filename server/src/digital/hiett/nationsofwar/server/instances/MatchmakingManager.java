package digital.hiett.nationsofwar.server.instances;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MatchmakingManager {

    private List<UUID> queue;
    private List<BattleMatch> games;

    public MatchmakingManager() {
        this.queue = new ArrayList<>();
        this.games = new ArrayList<>();
    }

    public List<BattleMatch> getGames() {
        return games;
    }

    public List<UUID> getQueue() {
        return queue;
    }

    public void addToQueue(UUID uuid) {
        this.queue.add(uuid);
    }

    public void removeFromQueue(UUID uuid) {
        this.queue.remove(uuid);
    }

    public boolean inQueue(UUID uuid) {
        return this.queue.contains(uuid);
    }

    public void updateQueue() {
        if (queue.size() == 0 || queue.size() == 1)
            return;

        // There is more than one person in the queue, process it.
        UUID a = queue.get(0);
        UUID b = queue.get(1);

        games.add(new BattleMatch(a, b));

        queue.remove(a);
        queue.remove(b);

        updateQueue(); // Update the queue again to remove any further people.
    }

    public BattleMatch getGame(UUID uuid) {
        for (BattleMatch g : games)
            if (g.getPlayerA().getUuid().equals(uuid) || g.getPlayerB().getUuid().equals(uuid))
                return g;

        return null;
    }

    public void removeMatch(BattleMatch match) {
        games.remove(match);
    }

    public boolean inGame(UUID uuid) {
        return getGame(uuid) != null;
    }

}
