package digital.hiett.nationsofwar.server;

import digital.hiett.nationsofwar.server.listeners.OnGameOver;
import digital.hiett.nationsofwar.server.users.User;
import digital.hiett.nationsofwar.server.users.UserManager;
import digital.hiett.nationsofwar.server.instances.MatchmakingManager;
import digital.hiett.nationsofwar.server.listeners.OnPlayerPlaceUnit;
import digital.hiett.nationsofwar.server.listeners.OnPlayerSelection;
import digital.hiett.nationsofwar.server.listeners.OnUserEnterMatchmaking;
import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.events.managers.EventManager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public class Main {

    public static final int PORT = 25565;
    public static final boolean IS_SERVER = true;

    // Object

    private ServerSocket serverSocket;
    private UserManager userManager;
    private MatchmakingManager matchmakingManager;
    private EventManager eventManager;

    public Main() throws IOException {
        System.out.println("Starting server.");

        this.serverSocket = new ServerSocket(PORT);
        this.userManager = new UserManager();
        this.matchmakingManager = new MatchmakingManager();
        this.eventManager = new EventManager();

        // Register events
        Event[] listeners = {
                new OnUserEnterMatchmaking(),
                new OnPlayerPlaceUnit(),
                new OnPlayerSelection(),
                new OnGameOver()
        };

        Arrays.asList(listeners).forEach(eventManager::registerEvent);
    }

    public void startSocket() throws IOException {
        while (true) {
            Socket socket = serverSocket.accept();
            User user = new User(socket);

            System.out.println("Client Connected!");

            // Add the user to the roster of users
            this.userManager.add(user);
        }
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public MatchmakingManager getMatchmakingManager() {
        return matchmakingManager;
    }

    public EventManager getEventManager() {
        return eventManager;
    }

    // Static Instance

    private static Main instance;

    public static void main(String[] args) {
        try {
            instance = new Main();
            instance.startSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Main getInstance() {
        return instance;
    }

}
