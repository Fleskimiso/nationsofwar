package digital.hiett.nationsofwar.outpost;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public enum OutpostTile {

    GRASS("Grass", "grass1", OutpostTileType.GROUND, 1),
    PLACE_GOOD_GRASS("Grass", "green-outline", OutpostTileType.GROUND, 1),
    PLACE_BAD_GRASS("Grass", "red-outline", OutpostTileType.GROUND, 1),
    SAND("Sand", "sand", OutpostTileType.GROUND, 1),
    WATER("Water", "water", OutpostTileType.GROUND, 1),
    TREE_1("Tree", "tree1", OutpostTileType.ITEM, 2),
    ROCK_1("Rock", "rock1", OutpostTileType.ITEM, 1),
    ROCK_2("Rock", "rock2", OutpostTileType.ITEM, 1),
    TREE_TEST("Test", "treetest", OutpostTileType.GROUND, 1);

    private Texture texture;
    private String name;
    private OutpostTileType outpostTileType;
    private int recommendedScale;

    OutpostTile(String name, String textureLocation, OutpostTileType outpostTileType, int recommendedScale) {
        this.recommendedScale = recommendedScale;
        this.outpostTileType = outpostTileType;
        this.texture = new Texture(Gdx.files.internal("tiles/" + textureLocation + ".png"));
        this.name = name;
    }

    public int getRecommendedScale() {
        return recommendedScale;
    }

    public OutpostTileType getOutpostTileType() {
        return outpostTileType;
    }

    public Texture getTexture() {
        return texture;
    }

    public String getName() {
        return name;
    }

}
