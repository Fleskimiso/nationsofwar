package digital.hiett.nationsofwar.outpost;

public class OutpostMap {

    private OutpostTile[][] baseMap = new OutpostTile[50][50];

    // Create a new map
    public OutpostMap() {
        for (int x = 0; x < 50; x++) {
            for (int y = 0; y < 50; y++) {
                OutpostTile tileToSet = OutpostTile.WATER;

                if (x > 20 && x < 30 && y > 20 && y < 30) {
                    tileToSet = OutpostTile.SAND;
                }

                if (x > 21 && x < 29 && y > 21 && y < 29) {
                    tileToSet = OutpostTile.GRASS;
                }

                baseMap[x][y] = tileToSet;
            }
        }
    }

    public OutpostTile[][] getBaseMap() {
        return baseMap;
    }

    public int getMapWidth() {
        return baseMap[0].length;
    }

    public int getMapHeight() {
        return baseMap.length;
    }

}
