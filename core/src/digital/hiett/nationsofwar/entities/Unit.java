package digital.hiett.nationsofwar.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public abstract class Unit {

    private Faction faction;
    private boolean playable;
    private Texture frontTexture, backTexture;
    private String description;
    private int health, damage;

    public Unit(Faction faction, boolean playable, String rawTextureName, String description, int health, int damage) {
        this.health = health;
        this.damage = damage;
        this.faction = faction;
        this.playable = playable;
        this.frontTexture = new Texture(Gdx.files.internal("units/" + rawTextureName + "_front.png"));
        this.backTexture = new Texture(Gdx.files.internal("units/" + rawTextureName + "_back.png"));
        this.description = description;
    }

    public int getDamage() {
        return damage;
    }

    public int getHealth() {
        return health;
    }

    public boolean isPlayable() {
        return playable;
    }

    public Texture getBackTexture() {
        return backTexture;
    }

    public Texture getFrontTexture() {
        return frontTexture;
    }

    public Faction getFaction() {
        return faction;
    }

    public String getDescription() {
        return description;
    }

    public abstract void onDamage();

    public abstract void onAttack();

}
