package digital.hiett.nationsofwar.entities.units;

import digital.hiett.nationsofwar.entities.Faction;
import digital.hiett.nationsofwar.entities.Unit;

public class Blitzer extends Unit {

    public Blitzer() {
        super(Faction.TEST, true, "empire/fire_fighter/fire_fighter", "Firefighter kinda guy", 20, 10);
    }

    @Override
    public void onDamage() {

    }

    @Override
    public void onAttack() {

    }

}
