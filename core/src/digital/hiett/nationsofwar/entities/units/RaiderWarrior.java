package digital.hiett.nationsofwar.entities.units;

import digital.hiett.nationsofwar.entities.Faction;
import digital.hiett.nationsofwar.entities.Unit;

public class RaiderWarrior extends Unit {

    public RaiderWarrior() { // units\raiders\radier_warrior/raider_warrior_front
        super(Faction.TEST, true, "raiders/radier_warrior/raider_warrior",
                "Standard Raider Trooper", 15, 5);
    }

    @Override
    public void onDamage() {
        System.out.println("Raider Warrior took damage");
    }

    @Override
    public void onAttack() {
        System.out.println("Raider Warrior attacks");
    }

}
