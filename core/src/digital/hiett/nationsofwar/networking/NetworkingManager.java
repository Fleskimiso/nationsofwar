package digital.hiett.nationsofwar.networking;

import com.badlogic.gdx.Gdx;
import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.packets.Packet;
import digital.hiett.nationsofwar.shared.packets.ReadPacket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class NetworkingManager {

    private Socket clientSocket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private Thread thread;

    public NetworkingManager() throws IOException {
        this.clientSocket = new Socket(Main.IP, 25565);
        this.dataInputStream = new DataInputStream(clientSocket.getInputStream());
        this.dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

        this.thread = new Thread(() -> {
            while (!this.clientSocket.isConnected()) {
                try {
                    Thread.sleep(1000 / 60);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("Connected to server!");

            try {
                while (true) {
                    System.out.println("Waiting on data...");
                    String data = dataInputStream.readUTF();
                    System.out.println(data);

                    ReadPacket readPacket = processPacket(data);

                    for (Event e : Main.getInstance().getEventManager().getEvents())
                        for (Class<? extends Event> e1 : readPacket.getPacket().getEvents())
                            if (e1.isAssignableFrom(e.getClass()))
                                e.handleEvent(readPacket);

//                    Main.getInstance().getEventManager().getEvents()
//                            .forEach(e -> Arrays.stream(readPacket.getPacket().getEvents())
//                                    .filter(e1 -> e1.isAssignableFrom(e.getClass()))
//                                    .forEach(e1 -> e.handleEvent(readPacket)));
                }
            } catch (IOException e) {
                // They have disconnected
                System.out.println("Server disconnected.");
                try {
                    dataOutputStream.close();
                    dataInputStream.close();
                    clientSocket.close();
                } catch (IOException e1) {
                    System.out.println("Couldn't close a stream or socket.");
                    e1.printStackTrace();
                }
                Gdx.app.exit();
            }
        });

        this.thread.start();
    }

    public void sendPacket(String packet) {
        try {
            System.out.println("Sending packet: " + packet);
            dataOutputStream.writeUTF(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ReadPacket processPacket(String packet) {
        return Packet.processPacket(packet);
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public DataInputStream getDataInputStream() {
        return dataInputStream;
    }

    public DataOutputStream getDataOutputStream() {
        return dataOutputStream;
    }

}
