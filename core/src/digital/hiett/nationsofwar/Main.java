package digital.hiett.nationsofwar;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import digital.hiett.nationsofwar.entities.LocalPlayer;
import digital.hiett.nationsofwar.networking.GameManager;
import digital.hiett.nationsofwar.networking.NetworkingManager;
import digital.hiett.nationsofwar.networking.listeners.*;
import digital.hiett.nationsofwar.screens.HomeScreen;
import digital.hiett.nationsofwar.screens.LoadingScreen;
import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.events.managers.EventManager;

import java.io.IOException;
import java.util.Arrays;

public class Main extends Game {

    public static String IP = "localhost";

    private LocalPlayer localPlayer;
    private EventManager eventManager;
    private NetworkingManager networkingManager;
    private GameManager gameManager;
    private boolean shouldChangeToGame;

    public Main(String mIp) {
        IP = mIp;
    }

    @Override
    public void create() {
        instance = this;

        this.localPlayer = new LocalPlayer();
        this.eventManager = new EventManager();
        this.gameManager = new GameManager();

        this.setScreen(new HomeScreen());
//        this.setScreen(new TestScreen());
//        this.setScreen(new LoadingScreen("Connecting to the server..."));

        try {
            this.networkingManager = new NetworkingManager();
        } catch (IOException e) {
            System.out.println("Couldn't connect to server! Closing app.");
            Gdx.app.exit();
        }

//        this.setScreen(new HomeScreen());

        Event[] events = {
                new OnUserDataReceive(),
                new OnGeneralQueueSize(),
                new OnOtherPlayerInfo(),
                new OnOtherPlayerPlaceUnit(),
                new OnSetPlayerGo(),
                new OnOtherPlayerAttack(),
                new OnThisPlayerDelete()
        };

        Arrays.asList(events).forEach(eventManager::registerEvent);

        this.shouldChangeToGame = false;
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        instance = null;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public boolean shouldChangeToGame() {
        return shouldChangeToGame;
    }

    public void setShouldChangeToGame(boolean shouldChangeToGame) {
        this.shouldChangeToGame = shouldChangeToGame;
    }

    public LocalPlayer getLocalPlayer() {
        return localPlayer;
    }

    public EventManager getEventManager() {
        return eventManager;
    }

    public NetworkingManager getNetworkingManager() {
        return networkingManager;
    }

    // Static Instance //

    private static Main instance;

    public static Main getInstance() {
        return instance;
    }

}
