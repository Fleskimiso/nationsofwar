package digital.hiett.nationsofwar.shared.utils;

public class Vector2Utils {

    public static SimpleVector2 enemyDataLocToGraphicsLoc(int x, int y) {
        y -= 4; // Make it so that it is inside the square.

        // 6, 6
        if (x == 6)
            x = 8;
        else if (x == 8)
            x = 6;

        if (y == 6)
            y = 8;
        else if (y == 8)
            y = 6;

        return new SimpleVector2(x, y);
    }

    public static SimpleVector2 enemyGraphicsLocToEnemyLoc(int x, int y) {
        y += 4; // Make it so that it is inside the square.

        // 6, 6
        if (x == 6)
            x = 8;
        else if (x == 8)
            x = 6;

        if (y == 6)
            y = 8;
        else if (y == 8)
            y = 6;

        return new SimpleVector2(x, y);
    }

}
