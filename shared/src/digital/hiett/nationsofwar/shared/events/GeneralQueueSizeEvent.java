package digital.hiett.nationsofwar.shared.events;

import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.packets.ReadPacket;

public abstract class GeneralQueueSizeEvent implements Event {

    @Override
    public void handleEvent(ReadPacket readPacket) {
        onEventCalled(Integer.parseInt(readPacket.getData()[0]));
    }

    public abstract void onEventCalled(int queueSize);

}
