package digital.hiett.nationsofwar.shared.events;

import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.packets.ReadPacket;

import java.util.UUID;

public abstract class GameOverEvent implements Event {

    @Override
    public void handleEvent(ReadPacket readPacket) {
        onEventCalled(readPacket.getSender());
    }

    public abstract void onEventCalled(UUID sender);

}
