package digital.hiett.nationsofwar.shared.events;

import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.packets.ReadPacket;

public abstract class ThisPlayerDelete implements Event {

    @Override
    public void handleEvent(ReadPacket readPacket) {
        onEventCalled(readPacket.getData()[0].equalsIgnoreCase("true"));
    }

    public abstract void onEventCalled(boolean isThis);

}
