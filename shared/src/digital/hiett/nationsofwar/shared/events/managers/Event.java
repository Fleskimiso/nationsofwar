package digital.hiett.nationsofwar.shared.events.managers;

import digital.hiett.nationsofwar.shared.packets.ReadPacket;

public interface Event {

    void handleEvent(ReadPacket readPacket);

}
