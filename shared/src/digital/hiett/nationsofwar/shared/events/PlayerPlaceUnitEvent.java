package digital.hiett.nationsofwar.shared.events;

import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.packets.ReadPacket;

import java.util.UUID;

public abstract class PlayerPlaceUnitEvent implements Event {

    @Override
    public void handleEvent(ReadPacket readPacket) {
        onEventCalled(readPacket.getSender(), Integer.parseInt(readPacket.getData()[0]),
                Integer.parseInt(readPacket.getData()[1]), Integer.parseInt(readPacket.getData()[2]),
                UUID.fromString(readPacket.getData()[3]));
    }

    public abstract void onEventCalled(UUID sender, int unitId, int x, int y, UUID uuid);

}
